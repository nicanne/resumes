# NicAnne Lamoureux, MBA, CSA(Cert)

## Summary

As a C-level right hand, I am the Great Glass Elevator of the leadership team, conveying information both across the organization and up/down between the leadership team and business teams, and external parties/stakeholders. I am a visual storyteller, helping others get on the same page through the use of stories and metaphors, straddling the line between the tactical and strategic. I ensure that there is the right environment for my executive and the leadership team to be, belong, and become. I am a passionate networking expert, building relationships across the business and beyond. I also exemplify the company culture, being a beacon that attracts new employees and makes existing employees want to remain with the business. I have unquestionable integrity and unshakable values, able to be an unwavering conscience and advisor for my principal.

## Skill Overview

- 20+ years supporting C-level executives including administration, operations, strategic planning, project and product management, proactive problem anticipation, identification and resolution, advisor and confidante for C-level executives across multiple industries
- Over 12 years experience in leadership/supervisory positions, managing teams with up to 14 direct reports
- Over 10 years experience as a Chief of Staff 
- Over 10 years experience in highly regulated, top-secret environments
- Past roles have included C-level partnerships that required operations, direct report management, data collection and analysis, Board liaison, relationship management (internal and external), project management, communications, budgets and forecasting, recruiting, human resources, employee engagement, problem solving, calendar/time management, priotization, travel planning, expense management, and countless more responsibilities as needed
- Decades of experience in confidential data management, information security, and ensuring confidentiality using the skills learned while right-hand to the Head of Compliance at an international financial institution
- Experience working in on-site, hybrid, and remote environments (I have a dedicated office in my home from which I work)
- Open to travel both domestically and internationally
- Available to align my work hours with any US time zone and work evenings/weekends when necessary

### Software Experience

- Microsoft Office (Word, PowerPoint, Excel, Visio, Outlook, OneNote, OneDrive, Booking)
- Google suite (calendar, docs, slides, sheets, forms)
- ChatGPT 4o, Genything, Grammarly, Zapier, Motion, Ambient, Calendly 
- Zoom, Teams, Slack, Google Meet 
- WorkDay, BambooHR, Greenhouse, Paylocity, CultureAmp 
- Salesforce, ServiceNow, Concur, SAP
- Asana, JIRA, Confluence, Monday.com, Notion
- Adobe, Canva, CultureAmp, SAP, Notion
- MacOS, Windows OS, iPad and iPhone, and more.

## Career Experience (Past 10 Years)

### Touchcast, Chief of Staff, Office of the CEO
11/2023 - present

_Touchcast is a growing tech startup on the bleeding edge of GenerativeAI technologies_

- Currently employed as Chief of Staff, Office of the CEO, at Touchcast
- I am right-hand and proxy for the CEO, working with globally distributed teams across the company as well as external clients, the Board, and investors to ensure excellence and efficiency
- I develop company-wide processes, fostering community and collaboration, while strategically managing the OCEO in all facets
- I directly partner with the Founder/CEO, managing all operational and administrative needs, including his travel, calendar, and cross-departmental special projects.

### Angel Assistance Group, LLC, Founder/Executive Director
08/2020 - present

_AAG is a small niche coaching a fractional support agency_

- Founded AAG as a virtual assistant service, but it has evolved over time to be focused on 1:1 coaching and corporate DEI consulting. I am responsible for all aspects of the company, including finance, sales, marketing, operations, website, recruiting/HR, etc.
- Work with clients on complex problems where considerable judgment and initiative are needed in resolving issues and making recommendations
- Serve as a liaison between executive clients and their stakeholders to facilitate discussions regarding employee satisfaction, gather 360 feedback, and assist in the implementation of changes
- Assist executives with the creation of PowerPoint presentations, financial spreadsheets, special reports, and other highly sensitive and confidential information
- Advisory consultant through Arootah, providing consulting services for HNW individuals and family offices
- Volunteer mentor for the Televerde Foundation and Delta Mu Delta

### Andela, Chief of Staff (laid off)
7/2022 - 12/2022

_Andela is a hypergrowth tech startup focused on SaaS_

- Provided strategic and operations support for the Chief Product & Technology Officer, including new initiatives to streamline Confluence and improve the onboarding process
- Collaborated with the CPTO to identify areas for improvement and implemented new procedures to enhance communication, increase employee engagement, establish guidelines and procedures, build relationships, and save costs
- Supported the 2023 budget (approx $20M )and headcount forecasts by analyzing and recommending training programs and budget allocation for employee development while identifying savings opportunities through credit card and software spend analysis, resulting in over $70K in annual savings in the first four weeks
- Organized and managed the department's calendar and offsite schedule for 2023, hosted monthly virtual lunches and brown bag sessions
- Completed white-glove on-boarding of and collaborated closely with the VP of Product, Head of Product Operations, VP of Data, VP of Engineering, VP of InfoSec, and VP of Growth/Marketing

### Clearco, Junior Chief of Staff - Strategy and Technology (layoffs)
11/2021 - 6/2022

_Clearco is a fintech unicorn startup based in Toronto, Canada_

- Provided fast and efficient operations and Chief of Staff support to the Chief Strategy Officer and Chief Technology Officer, including project management, recruiting, onboarding, budget analysis, time management, travel planning, and expense approval
- Successfully reduced candidate pipeline time by five days in less than three months, addressing a significant issue for the company and enhancing overall recruitment efficiency
- Collaborated closely with the CSO and CTO to optimize their workflow and streamline administrative operations, driving greater productivity and success for the company
- Project Manager for several high-impact cross-functional projects, including the launch of a new payments product for North American customers

### Aerial Electric, Senior Support Specialist (resigned to care for parent at end-of-life)
11/2020 - 7/2021

_Aerial is an electrical contractor and subsidiary of IES Holdings_

- Functioned as office manager, human resources manager, marketing director, contract reviewer, and project manager for the President and Head of Sales and Estimating
- Developed and executed the company's marketing strategy, producing hundreds of portfolios and establishing an online presence for the company on LinkedIn and Facebook
- Effectively managed two direct reports while overseeing various aspects of company operations and supporting executive leadership in achieving organizational goals

### Bridgewater Associates, Administration and Leverager/Chief of Staff (laid off)
09/2011-8/2020

_Bridgewater is the world's largest hedge fund with over $160B under management_

- Initially hired as an Administrative Assistant to the Head of End User Support (tech) through Pride Technologies on a 3-month contract, then converted to a full-time employee in June 2012
- Promoted to Executive Assistant of Executive Support (tech) in 2013 and further to Leverager (Chief of Staff) for the Finance Department Head (CFO) managing a team of 14 direct reports
- After the retirement of the CFO, I transfered to Trading Technology, where I supported the Head of Trading Tech in a top secret work environment
- Successfully developed and implemented a comprehensive onboarding and training program for all administrative staff
- Project manager on the Issue Reduction Project, which reduced open complaints directed to the finance department from over 3500 to under 300 within six months
- Provided strategic and operations support for the Chief Product & Technology Officer, including new initiatives to streamline Confluence and improve the onboarding process
- Collaborated with the CPTO to identify areas for improvement and implemented new procedures to enhance communication, increase employee engagement, establish guidelines and procedures, build relationships, and save costs
- Supported the 2023 budget (approx $20M) and headcount forecasts by analyzing and recommending training programs and budget allocation for employee development while identifying savings opportunities through credit card and software spend analysis, resulting in over $70K in annual savings in the first four weeks
- Organized and managed the department's calendar and offsite schedule for 2023, hosted monthly virtual lunches and brown bag sessions
- Completed white-glove on-boarding of and collaborated closely with the VP of Product, Head of Product Operations, VP of Data, VP of Engineering, VP of InfoSec, and VP of Growth/Marketing

## Education

- Capella University, Master of Science - Industrial and Organizational Psychology (4.0 GPA, current)
- University of Oxford, Said Business School (U.K.), Certified Chief of Staff
- Singapore Business School (Singapore), Master of Business Administration (Distinction)
- Southern New Hampshire University, Bachelor of Science - Business Administration (4.0 GPA, Summa cum laude)
- University of Canterbury (New Zealand), Bachelor of Teaching and Learning (3.43 GPA, incomplete)


## Work Samples

Virtual Resume (Beta):

- [View on my website](http://www.nicanne.com/virtual-resume)

  - Please note that this video has been captioned in English and Spanish, but some users have notified me of technical issues preventing the captions from showing. I am working to correct the technical issue and make this more accessible for hearing-impaired folx.

### Writing Samples: BSBA and MBA assignments
The following links are available on my website: [Writing Samples](http://www.nicanne.com/writing-samples)

- [Case Study Analysis and Strategy Proposal: ITC Limited](https://docs.google.com/document/d/1T9FVd1FCgKZ-rr8Edxii61dWkWjeXb8m/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)
- [Disenfranchisement of Black Women Voters (1920-1965)](https://docs.google.com/document/d/1qZNRd85wgS5c3yzCgRS2pnZyqWOTA2cW/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)
- [Diversification Analysis](https://docs.google.com/document/d/1YuMQYMo9DXqur7NqjfZP8fXzdlcqEaDH/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)
- [Diversification Presentation (PPT)](https://docs.google.com/presentation/d/1uZRQSgWVJrLt3DbR_GTd3jNXtlBMYNnz/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)
- [Diversity: Transgender Rights](https://docs.google.com/document/d/1ddmeWEDc6ziaha5Qw-XswnUK2GVgXI5v/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)
- [Human Resources Cost Reduction Analysis](https://docs.google.com/document/d/1yM7xq6EweklCI8qfrWgHqxoFUzmGrFsp/edit?usp=sharing&ouid=107705198133089497131&rtpof=true&sd=true)

### Lyft Case Study (beta content: captions currently not functioning as expected)

- [View on my website](http://www.nicanne.com/casestudy-lyft)

### Master’s Presentation (beta):
Avoiding Bias in Recruiting

- [Link to video](https://tc.touchcast.com/s/74eunh1ajg) 
  - Password: @CapellaU2024
